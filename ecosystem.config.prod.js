export default {
    apps: [{
        name: 'bet-backend',
        script: 'dist/app.js',
        watch: false,
    }],
};
