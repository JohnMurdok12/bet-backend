import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

// Prisma needs to be a singleton
export default prisma;