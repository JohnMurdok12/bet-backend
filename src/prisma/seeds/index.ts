import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

const runSeeds = async (): Promise<void> => {
    await prisma.sport.create({
        data: {
            id: 1,
            name: 'Football',
            competitions: {
                create: [
                    {
                        name: 'Ligue 1',
                        country: 'fr',
                        teams: {
                            create: [{
                                name: 'PSG',
                                players: {
                                    create: [{
                                        player: {
                                            create: {
                                                name: 'Kylian Mbappe',
                                            },
                                        },
                                        isCurrent: true,
                                    }],
                                },
                            }],
                        },
                    }, {
                        name: 'Ligue 2',
                        country: 'fr',
                        teams: {
                            create: [{
                                name: 'Toulouse FC',
                                players: {
                                    create: [{
                                        player: {
                                            create: {
                                                name: 'Rhys Healy',
                                            },
                                        },
                                        isCurrent: true,
                                    }],
                                },
                            }],
                        },
                    },
                ],
            },
        },
    });

    await prisma.betType.createMany({
        data: [{
            id: 1,
            name: 'SIMPLE',
        }, {
            id: 2,
            name: 'COMBINED',
        }, {
            id: 3,
            name: 'SYSTEM',
        }, {
            id: 4,
            name: 'BOOSTED_BET',
        }, {
            id: 5,
            name: 'SUPER_BOOSTED_BET',
        }],
    });

    await prisma.eventBetType.createMany({
        data: [{
            id: 1,
            name: 'RESULT',
        }, {
            id: 2,
            name: 'DOUBLE_CHANCE',
        }, {
            id: 3,
            name: 'REFUND_IF_DRAW',
        }, {
            id: 4,
            name: 'SCORER',
        }],
    });
};

runSeeds();
