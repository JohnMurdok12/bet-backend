-- DropForeignKey
ALTER TABLE `accountoperation` DROP FOREIGN KEY `AccountOperation_operationTypeId_fkey`;

-- AlterTable
ALTER TABLE `accountoperation` DROP COLUMN `operationTypeId`,
    ADD COLUMN `operationType` ENUM('DEPOSIT', 'WITHDRAW', 'FREE_BET') NOT NULL;

-- DropTable
DROP TABLE `accountoperationtype`;
