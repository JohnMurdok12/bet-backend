-- CreateTable
CREATE TABLE `BetPart` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `betId` INTEGER NOT NULL,
    `rate` DOUBLE NOT NULL,
    `externalId` VARCHAR(36),
    `status` ENUM('CANCELED', 'NOT_STARTED', 'WON', 'LOST') NOT NULL DEFAULT 'NOT_STARTED',
    `eventBetId` INTEGER NOT NULL,

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `EventBet` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `subTypeId` INTEGER NOT NULL,
    `eventId` INTEGER NOT NULL,
    `rate` DOUBLE NOT NULL,
    `resourceType` ENUM('TEAM', 'PLAYER') NOT NULL DEFAULT 'TEAM',
    `resourceId` INTEGER NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedAt` DATETIME(3),

    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `BetPart` ADD CONSTRAINT `BetPart_betId_fkey` FOREIGN KEY (`betId`) REFERENCES `Bet`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `BetPart` ADD CONSTRAINT `BetPart_eventBetId_fkey` FOREIGN KEY (`eventBetId`) REFERENCES `EventBet`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `EventBet` ADD CONSTRAINT `EventBet_subTypeId_fkey` FOREIGN KEY (`subTypeId`) REFERENCES `EventBetSubtype`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `EventBet` ADD CONSTRAINT `EventBet_eventId_fkey` FOREIGN KEY (`eventId`) REFERENCES `Event`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;
