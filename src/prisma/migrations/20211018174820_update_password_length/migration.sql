-- AlterTable
ALTER TABLE `user` MODIFY `password` VARCHAR(512) NOT NULL,
    MODIFY `salt` VARCHAR(64) NOT NULL;
