import IAccountOperation from '@interfaces/model/IAccountOperation';
import accountRepository from '@repositories/account';
import accountOperationRepository from '@repositories/accountOperation';

class AccountOperationService {
    /**
     * Create account operation and update account balance
     * @param {number} accountId
     * @param {Partial<IAccountOperation>} data 
     * @param {number} newBalance 
     * @returns {Promise<IAccountOperation>}
     */
    async createAndUpdateAccountBalance(
        accountId: number,
        data: Partial<IAccountOperation>,
        newBalance: number,
    ): Promise<IAccountOperation> {
        const accountOperation = await accountOperationRepository.create({
            ...data,
            accountId,
        });
    
        // update account with new balance
        await accountRepository.updateByFields({
            currentBalance: newBalance, updatedAt: new Date(),
        }, { id: accountId });

        return accountOperation;
    }
}

export default new AccountOperationService();
