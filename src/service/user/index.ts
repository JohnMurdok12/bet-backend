import IUser from '@interfaces/model/IUser';
import userRepository from '@repositories/user';

class UserService {
    /**
     * Get user by uuid
     * @param uuid
     * @returns user
     */
    getByUuid(uuid: string): Promise<Partial<IUser>> {
        return userRepository.findOneByFields({ uuid }, {
            id: true,
            uuid: true,
            username: true,
            email: true,
            role: true,
            createdAt: true,
            updatedAt: true,
        });
    }
}

export default new UserService();
