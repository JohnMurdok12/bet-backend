import * as crypto from 'crypto';
import { hashPassword, checkPassword, IPassword } from '@utils/password';

jest.mock('crypto');

const pbkdf2SyncMock = crypto.pbkdf2Sync as unknown as jest.Mock<Buffer>;
const randomBytesMock = crypto.randomBytes as unknown as jest.Mock<Buffer>;

pbkdf2SyncMock.mockReturnValue(Buffer.from('test'));
randomBytesMock.mockReturnValue(Buffer.from('128'));

describe('[UTIL] Password - hashPassword()', () => {
    test('should hash password and return object with hashed password and salt', () => {
        const passwordData: IPassword = hashPassword('test');

        expect(passwordData).toMatchObject({
            password: '74657374',
            salt: '313238',
        });
    });
});

describe('[UTIL] Password - checkPassword()', () => {
    test('should return false if password is not the same', () => {
        pbkdf2SyncMock.mockReturnValueOnce(Buffer.from('zonzon'));

        const result: Boolean = checkPassword('zonzon', '74657374', 'st313238op');
        expect(result).toEqual(false);
    });

    test('should return true if password is the same', () => {
        const result: Boolean = checkPassword('test', '74657374', '313238');
        expect(result).toEqual(true);
    });
});
