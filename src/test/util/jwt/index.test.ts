import jwt from 'jsonwebtoken';
import { encode as encodeToken, decode as decodeToken, IDecodedToken } from '@utils/jwt';

jest.mock('jsonwebtoken');
const signMock = jwt.sign as unknown as jest.Mock<String>;
const verifyMock = jwt.verify as unknown as jest.Mock<IDecodedToken>;

const dateMock = new Date('2021-10-18T21:26:00.000Z');
jest.spyOn(global, 'Date')
    .mockImplementation(() => (dateMock as unknown) as string);

describe('[UTILS][JWT] encode()', () => {
    test('should return an object with token and expiresAt', () => {
        signMock.mockReturnValueOnce('token');
        const result = encodeToken('uuid');
        const expiresAt = new Date();
        expiresAt.setDate(dateMock.getDate() + 1);

        expect(result).toMatchObject({
            token: 'token',
            expiresAt,
        });
        expect(signMock).toHaveBeenCalledWith({
            uuid: 'uuid',
            expiresAt: '2021-10-19T21:26:00.000Z',
        }, process.env.JWT_SECRET, {
            algorithm: 'HS256',
        });
    });
});

describe('[UTILS][JWT] decode()', () => {
    test('should return an object with uuid and expiresAt', () => {
        verifyMock.mockReturnValueOnce({
            uuid: 'uuid',
            expiresAt: '2021-10-18T21:26:00.000Z',
        });

        const result = decodeToken('token');
        expect(result).toMatchObject({
            uuid: 'uuid',
            expiresAt: '2021-10-18T21:26:00.000Z',
        });
        expect(verifyMock).toHaveBeenCalledWith('token', process.env.JWT_SECRET);
    });
});
