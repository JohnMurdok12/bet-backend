interface IMockModelAttributes {
    name?: boolean
}
export { IMockModelAttributes };
export default interface IMockModel {
    name: string
}