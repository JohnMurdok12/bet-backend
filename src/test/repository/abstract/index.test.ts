import AbstractRepository from '@repositories/abstract';
import IMockModel, { IMockModelAttributes } from './fixtures';

const model = {
    create: jest.fn(),
    findFirst: jest.fn(),
    findMany: jest.fn(),
    update: jest.fn(),
};

class Repository extends AbstractRepository<IMockModel, IMockModelAttributes> {}
const repository = new Repository(model);

describe('[REPOSITORY] AbstractRepository', () => {
    test('create() - should call create model method', async () => {
        model.create.mockResolvedValueOnce({ name: 'toto' });
        const data = await repository.create({ name: 'toto' });
        expect(data).toMatchObject({ name: 'toto' });
        expect(model.create).toHaveBeenCalledWith({
            data: {
                name: 'toto',
            },
        });
    });

    test('findOneByFields() - should call findFirst on model', async () => {
        model.findFirst.mockResolvedValueOnce({ name: 'toto' });
        const data = await repository.findOneByFields({ name: 'toto' }, { name: true });
        expect(data).toMatchObject({ name: 'toto' });
        expect(model.findFirst).toHaveBeenCalledWith({
            where: { name: 'toto' },
            select: { name: true },
        });
    });

    test('updateByFields() - should call update with where fields', async () => {
        model.update.mockResolvedValueOnce({ name: 'totem' });
        const data = await repository.updateByFields({ name: 'totem' }, { name: 'toto' });
        expect(data).toMatchObject({ name: 'totem' });
        expect(model.update).toHaveBeenCalledWith({
            data: { name: 'totem' },
            where: { name: 'toto' },
        });
    });

    test('findByFilters() - should call findMany', async () => {
        model.findMany.mockResolvedValueOnce([{ name: 'totem' }]);
        const data = await repository.findByFilters({
            where: {
                id: 1,
            },
        });
        expect(data).toMatchObject([{ name: 'totem' }]);
        expect(model.findMany).toHaveBeenCalledWith({
            where: {
                id: 1,
            },
        });
    });
});