import dotenvSafe from 'dotenv-safe';
import path from 'path';

dotenvSafe.config({
    path: path.resolve(process.cwd(), '.env.test'),
});
