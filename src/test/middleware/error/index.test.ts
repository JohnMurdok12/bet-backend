import errorMiddleware from '@middlewares/error';

const res = {
    status: jest.fn(),
    send: jest.fn(),
} as any;
res.status.mockReturnValue(res);
res.send.mockImplementation((value: string) => value);

jest.spyOn(console, 'error').mockImplementationOnce(() => null);

describe('[MIDDLEWARE] errorMiddleware()', () => {
    test('should return err message and stack', () => {
        const error = {
            status: 404,
            message: 'message',
            stack: 'stack',
        };
        errorMiddleware(error, null, res, jest.fn());
        expect(res.status).toHaveBeenCalledWith(404);
        expect(res.send).toHaveBeenCalledWith({ message: 'message', stack: 'stack' });
    });
});
