import authMiddleware from '@middlewares/auth';
import { decode as decodeToken, IDecodedToken } from '@utils/jwt';
import userService from '@services/user';
import IUser from '@interfaces/model/IUser';

jest.mock('@utils/jwt');
jest.mock('@services/user');

const decodeTokenMock = decodeToken as unknown as jest.Mock<IDecodedToken>;
const getUserByUuidMock = userService.getByUuid as unknown as jest.Mock<Promise<Partial<IUser>>>;
const fixtures = {
    req: {
        headers: {},
    },
    res: {} as any,
    next: jest.fn((e) => e),
};

jest
    .useFakeTimers()
    .setSystemTime(new Date('2021-10-23T01:00:00.000Z').getTime());

describe('[MIDDLEWARE] authMiddleware()', () => {
    test('should go next if token not exists', async () => {
        const { req, res, next } = fixtures;
        await authMiddleware(req as any, res, next);
        expect(next).toHaveBeenCalledTimes(1);
        expect(next).toHaveBeenCalledWith();
    });


    test('should throw error if expires time < current date time', async () => {
        const { req, res, next } = fixtures;
        const request = {
            ...req,
            headers: {
                authorization: 'token',
            },
        };

        decodeTokenMock.mockReturnValue({ uuid: 'uuid', expiresAt: '2021-10-23T00:00:00Z' });
        const result = await authMiddleware(request as any, res, next);
        expect(result.message).toEqual('Token has expired');
        expect(decodeTokenMock).toHaveBeenCalledWith('token');
    });

    test('should throw error if user is not found and token valid', async () => {
        const { req, res, next } = fixtures;
        const request = {
            ...req,
            headers: {
                authorization: 'token',
            },
        };

        decodeTokenMock.mockReturnValue({ uuid: 'uuid', expiresAt: '2021-10-24T00:00:00Z' });
        getUserByUuidMock.mockResolvedValueOnce(null);
        const result = await authMiddleware(request as any, res, next);
        expect(result.message).toEqual('User not found');
        expect(getUserByUuidMock).toHaveBeenCalledWith('uuid');
        expect(decodeTokenMock).toHaveBeenCalledWith('token');
    });

    test('should go next if token is valid and user is found', async () => {
        const { req, next } = fixtures;
        const request = {
            ...req,
            headers: {
                authorization: 'token',
            },
        };
        const response = {
            locals: {},
        } as any;

        decodeTokenMock.mockReturnValue({ uuid: 'uuid', expiresAt: '2021-10-24T00:00:00Z' });
        getUserByUuidMock.mockResolvedValueOnce({ uuid: 'uuid' });

        await authMiddleware(request as any, response, next);
        expect(response.locals).toMatchObject({
            user: { uuid: 'uuid' },
        });
        expect(decodeTokenMock).toHaveBeenCalledWith('token');
        expect(getUserByUuidMock).toHaveBeenCalledWith('uuid');
    });
});
