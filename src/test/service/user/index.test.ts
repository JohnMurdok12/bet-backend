import IUser from '@interfaces/model/IUser';
import userService from '@services/user';
import userRepository from '@repositories/user';

jest.mock('@repositories/user');
const findOneByFieldsMock = userRepository.findOneByFields as unknown as jest.Mock<Promise<Partial<IUser>>>;

describe('[SERVICE] User', () => {
    describe('getByUuid()', () => {
        test('should call userRepository and return user', async () => {
            findOneByFieldsMock.mockResolvedValueOnce({ id: 1 });
            const result = await userService.getByUuid('uuid');
            expect(result).toMatchObject({ id: 1 });
            expect(findOneByFieldsMock).toHaveBeenCalledWith({ uuid: 'uuid' }, {
                id: true,
                uuid: true,
                username: true,
                email: true,
                role: true,
                createdAt: true,
                updatedAt: true,
            });
        });
    });
});
