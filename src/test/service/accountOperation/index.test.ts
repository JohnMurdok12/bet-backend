import IAccountOperation from '@interfaces/model/IAccountOperation';
import accountRepository from '@repositories/account';
import accountOperationRepository from '@repositories/accountOperation';
import accountOperationService from '@services/accountOperation';

jest.mock('@repositories/account');
jest.mock('@repositories/accountOperation');
jest
    .useFakeTimers()
    .setSystemTime(new Date('2021-10-23T01:00:00.000Z').getTime());

const accountUpdateByFields = accountRepository.updateByFields as unknown as jest.Mock<Promise<null>>;
const createOperation = accountOperationRepository.create as unknown as jest.Mock<Promise<Partial<IAccountOperation>>>;


describe('[SERVICE] AccountOperationService', () => {
    test('createAndUpdateAccountBalance() - should insert data and update account balance', async () => {
        accountUpdateByFields.mockResolvedValue(null);
        createOperation.mockResolvedValue({
            id: 34,
            operationType: 'WITHDRAW',
            amount: 12,
            accountId: 1,
        });

        const data = await accountOperationService.createAndUpdateAccountBalance(
            1, { operationType: 'WITHDRAW', amount: 12 }, 36.92,
        );
        expect(data).toMatchObject({
            id: 34,
            operationType: 'WITHDRAW',
            amount: 12,
            accountId: 1,
        });

        expect(createOperation).toHaveBeenCalledWith({
            operationType: 'WITHDRAW',
            amount: 12,
            accountId: 1,
        });

        expect(accountUpdateByFields).toHaveBeenCalledWith(
            { currentBalance: 36.92, updatedAt: new Date('2021-10-23T01:00:00.000Z') },
            { id: 1 },
        );
    });
});
