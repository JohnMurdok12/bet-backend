import { mapSchema, getDirective, MapperKind } from '@graphql-tools/utils';
import { GraphQLSchema } from 'graphql';
import requireAuthDirective from '@resolvers/directive/requireAuth';
jest.mock('@graphql-tools/utils');

interface IDirectiveResolver {
    resolve: Function,
}

interface IDirectiveMock {
    [MapperKind.OBJECT_FIELD]: Function,
}

const mapSchemaMock = mapSchema as unknown as jest.Mock<Object>;
const getDirectiveMock = getDirective as unknown as jest.Mock<Object[]>;
const resolveMock = jest.fn();

mapSchemaMock.mockImplementationOnce((schema, directive) => directive);
getDirectiveMock.mockImplementationOnce(() => [{}]);
const fieldConfig = { id: 'test', resolve: resolveMock };
const schemaDirective = requireAuthDirective({} as unknown as GraphQLSchema, 'requireAuth') as unknown as IDirectiveMock;
const directiveFn = schemaDirective[MapperKind.OBJECT_FIELD];
const directive = directiveFn(fieldConfig) as unknown as IDirectiveResolver;

describe('[RESOLVER][DIRECTIVE] requireAuthDirective()', () => {
    test('should throw HttpError when resolver has no user in context', () => {
        expect(directive.resolve).toBeDefined();
        const info = {
            path: {
                key: 'Login',
                typename: 'mutation',
            },
        };
        expect(() => directive.resolve(null, null, {
            user: null,
        }, info)).toThrow('Client must be identified to request mutation: Login');

        expect(resolveMock).toHaveBeenCalledTimes(0);
    });

    test('should resolve request when resolver has user in context', () => {
        const context = {
            user: { id: 1 },
        };
        directive.resolve(null, null, context, null);
        expect(resolveMock).toHaveBeenCalledTimes(1);
    });
});
