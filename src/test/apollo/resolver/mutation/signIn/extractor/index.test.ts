import signInExtractor from '@resolvers/mutation/signIn/extractor';
import { hashPassword, IPassword } from '@utils/password';
jest.mock('@utils/password');

const hashPasswordMock = hashPassword as unknown as jest.Mock<IPassword>;
hashPasswordMock.mockImplementationOnce(() => ({
    salt: '123456',
    password: 'pwd',    
}));

describe('[RESOLVER][MUTATION] signInExtractor()', () => {
    test('should return an object', () => {
        expect(signInExtractor({
            username: 'test',
            password: 'a',
            email: 'toto@toto.fr',
            externalId: '1',
            currentBalance: 12.1,
        })).toMatchObject({
            username: 'test',
            email: 'toto@toto.fr',
            externalId: '1',
            currentBalance: 12.1,
            password: 'pwd',
            salt: '123456',
            role: 'USER',
        });
    });
});
