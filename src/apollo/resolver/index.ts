import { resolvers } from 'graphql-scalars';
import User from './type/user';
import fileService from '@services/file';

export default {
    ...resolvers,
    User,
    Query: fileService.getGraphQLResolversFromPath(`${__dirname}/query`, ['hydrator']),
    Mutation: fileService.getGraphQLResolversFromPath(`${__dirname}/mutation`, ['extractor']),
};
