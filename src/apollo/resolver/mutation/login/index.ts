import ILoginInput, { ILogin } from '@interfaces/request/mutation/ILogin';
import IUser, { IUserAttributes } from '@interfaces/model/IUser';
import userRepository from '@repositories/user';
import { encode as encodeToken } from '@utils/jwt';
import { checkPassword } from '@utils/password';


/**
 * @param {null} _
 * @param {ILoginInput} login
 * @returns {Promise<ILogin>}
 */
export default async (
    _: null,
    input: ILoginInput,
): Promise<ILogin> => {
    const fields = { username: input.username } as Partial<IUser>;
    const attributes = {
        uuid: true,
        username: true,
        password: true,
        salt: true,
    } as IUserAttributes;
    const user = await userRepository.findOneByFields(fields, attributes);

    if (!user || !checkPassword(input.password, user.password, user.salt)) {
        throw new Error('User don\'t exist or wrong password');
    }

    return encodeToken(user.uuid);
};

