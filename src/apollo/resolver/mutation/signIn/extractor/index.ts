import { ISignInUserInputData, ISignInUserCreate } from '@interfaces/request/mutation/ISignInUser';
import { hashPassword } from '@utils/password';
import { UserRole } from '@prisma/client';

/**
 * Extractor from input data
 * @param user 
 * @returns {ISignInUserCreate}
 */
const signInExtractor = (user: ISignInUserInputData): ISignInUserCreate => ({
    ...user,
    ...hashPassword(user.password),
    role: UserRole.USER,
});

export default signInExtractor;
