import ISignInUserInput from '@interfaces/request/mutation/ISignInUser';
import extractor from './extractor';
import userRepository from '@repositories/user';
import IUser from '@interfaces/model/IUser';

/**
 * @param {null} _
 * @param {IUser} input
 * @returns {Promise<IUser>}
 */
export default (
    _: null,
    input: ISignInUserInput,
): Promise<IUser> => userRepository.create(extractor(input.data));
