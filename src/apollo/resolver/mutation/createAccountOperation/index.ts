import IAccountOperationCreateInput from '@interfaces/request/mutation/IAccountOperation';
import IAccountOperation from '@interfaces/model/IAccountOperation';
import accountRepository from '@repositories/account';
import accountOperationService from '@services/accountOperation';
import IApolloContext from '@interfaces/apollo/context';
import { AccountOperationType } from '@prisma/client';
import HttpError from 'error/http';
import httpStatus from 'http-status';

/**
 * @param {null} _
 * @param {IAccountOperationCreateInput} input
 * @returns {Promise<IUser>}
 */
export default async (
    _: null,
    input: IAccountOperationCreateInput,
    ctx: IApolloContext,
): Promise<IAccountOperation> => {
    const { data } = input;
    const account = await accountRepository.findOneByFields({
        id: ctx.user.account.id,
        userId: ctx.user.id,
    }, {
        id: true,
        currentBalance: true,
    });

    let newBalance: number = account.currentBalance;
    switch (data.operationType) {
        case AccountOperationType.WITHDRAW:
            newBalance -= data.amount;
            break;
        case AccountOperationType.DEPOSIT:
        case AccountOperationType.FREE_BET:
        default:
            newBalance += data.amount;
            break;
    }

    if (newBalance < 0) {
        throw new HttpError(httpStatus.FORBIDDEN, 'Balance of account will be under 0');
    }

    return accountOperationService.createAndUpdateAccountBalance(
        account.id, data, newBalance,
    );
};
