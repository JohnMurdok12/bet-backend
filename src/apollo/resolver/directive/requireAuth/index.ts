import { mapSchema, getDirective, MapperKind } from '@graphql-tools/utils';
import {
    GraphQLSchema, GraphQLFieldConfig, GraphQLFieldResolver, GraphQLResolveInfo,
} from 'graphql';
import IApolloContext from '@interfaces/apollo/context';
import httpStatus from 'http-status';
import HttpError from '../../../../error/http';

/**
 * Overrride a given resolver
 * @param {GraphQLFieldResolver<any, any>} resolve
 * @returns {GraphQLFieldResolver<any, any>}
 */
const getResolver = (resolve: GraphQLFieldResolver<any, any>) => (
    source: null, args: null, context: IApolloContext, info: GraphQLResolveInfo,
) => {
    if (!context.user) {
        const { typename: type, key } = info.path;
        throw new HttpError(
            httpStatus.FORBIDDEN,
            `Client must be identified to request ${type}: ${key}`,
        );
    }

    return resolve(source, args, context, info);
};

/**
 * Resolve requireAuth directive
 * @param {GraphQLSchema} schema
 * @param {string} directiveName
 * @returns {GraphQLSchema}
 */
export default (schema: GraphQLSchema, directiveName: string) => mapSchema(schema, {
    [MapperKind.OBJECT_FIELD]: (fieldConfig: GraphQLFieldConfig<any, any>) => {
        const directive = getDirective(schema, fieldConfig, directiveName)?.[0];
        if (directive) {
            const resolve = getResolver(fieldConfig.resolve);
            return { ...fieldConfig, resolve };
        }

        return fieldConfig;
    },
});
