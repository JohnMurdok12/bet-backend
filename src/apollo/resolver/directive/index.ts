import { GraphQLSchema } from 'graphql';
import requireAuthResolver from './requireAuth';
/**
 * @param {GraphQLSchema} schema
 * @returns {GraphQLSchema}
 */
export default (schema: GraphQLSchema) => {
    let schemaWithDirectives = schema;

    schemaWithDirectives = requireAuthResolver(
        schema,
        'requireAuth',
    );

    return schemaWithDirectives;
};
