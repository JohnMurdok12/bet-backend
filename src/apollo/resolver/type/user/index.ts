import IAccount from '@interfaces/model/IAccount';
import IApolloContext from '@interfaces/apollo/context';
import accountRepository from '@repositories/account';

/**
 * @param {null} _
 * @param {null} input
 * @param {IApolloContext} ctx
 * @returns {IUser}
 */
const account = (
    _: null,
    input: null,
    ctx: IApolloContext,
): Promise<Partial<IAccount>> => accountRepository.findOneByFields(
    { userId: ctx.user.id, id: ctx.user.account.id },
    {
        id: true,
        currentBalance: true,
        createdAt: true,
        updatedAt: true,
        externalId: true,
    },
);

export default {
    account,
};
