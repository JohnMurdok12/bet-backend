import IUser from '@interfaces/model/IUser';
import IApolloContext from '@interfaces/apollo/context';

/**
 * @param {null} _
 * @param {null} input
 * @param {ExpressContext} ctx
 * @returns {IUser}
 */
export default (
    _: null,
    input: null,
    ctx: IApolloContext,
): Partial<IUser> => ctx.user;
