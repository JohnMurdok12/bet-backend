import { GetAccountOperationsFilters } from '@interfaces/request/query/IGetAccountOperations';
import { IAccountOperationsFilters } from '@interfaces/model/IAccountOperation';

const filterHydrator = (accountId: number, filters: GetAccountOperationsFilters): IAccountOperationsFilters => ({
    where: {
        accountId,
        operationType: { in: [filters.operationType] },
    },
});

export default filterHydrator;
