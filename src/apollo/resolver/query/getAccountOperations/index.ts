import { IAccountOperationsFilters } from '@interfaces/model/IAccountOperation';
import GetAccountOperationsInput, {
    GetAccountOperationsResponse,
} from '@interfaces/request/query/IGetAccountOperations';
import IApolloContext from '@interfaces/apollo/context';
import accountOperationRepository from '@repositories/accountOperation';
import paginationHydrator from 'hydrator/pagination';
import filterHydrator from './hydrator';


/**
 * @param {null} _
 * @param {null} input
 * @param {ExpressContext} ctx
 * @returns {IUser}
 */
export default async (
    _: null,
    input: GetAccountOperationsInput,
    ctx: IApolloContext,
): Promise<GetAccountOperationsResponse> => {
    const { pagination } = input;
    const filters: IAccountOperationsFilters = {
        ...filterHydrator(ctx.user.account.id, input.filters),
        ...paginationHydrator(pagination),
    };

    const operations = await accountOperationRepository.findByFilters(filters);
    return {
        cursor: operations[pagination.limit - 1]?.id || null,
        operations,
    };
};
