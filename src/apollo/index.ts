import { ApolloServer } from 'apollo-server-express';
import { GraphQLSchema } from 'graphql';
import { makeExecutableSchema } from '@graphql-tools/schema';
import { typeDefs as scalarTypeDefs } from 'graphql-scalars';
import IApolloContext from '@interfaces/apollo/context';
import resolveWithDirectives from './resolver/directive';
import fileService from '../service/file';
import resolvers from './resolver';
const graphqlSchema: string = fileService.getGraphQLSchemaFromPath(`${__dirname}/schema`);

const schema: GraphQLSchema = makeExecutableSchema({
    typeDefs: [graphqlSchema, ...scalarTypeDefs],
    resolvers,
});

const apolloServer: ApolloServer = new ApolloServer({
    context: ({ res }) => ({
        user: res.locals.user,
    }) as IApolloContext,
    schema: resolveWithDirectives(schema),
});

export default apolloServer;
