import prismaClient from '../../prisma/client';
import AbstractRepository from '@repositories/abstract';
import IAccount, { IAccountAttributes } from '@interfaces/model/IAccount';

class AccountRepository extends AbstractRepository<IAccount, IAccountAttributes> {
}

export default new AccountRepository(
    prismaClient.account,
);
