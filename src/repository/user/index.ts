import prismaClient from '../../prisma/client';
import AbstractRepository from '@repositories/abstract';
import { ISignInUserCreate } from '@interfaces/request/mutation/ISignInUser';
import IUser, { IUserAttributes } from '@interfaces/model/IUser';

class UserRepository extends AbstractRepository<IUser, IUserAttributes> {
    /**
     * Create user on sign in action
     * @override
     * @param user
     * @returns {Promise<IUser>}
     */
    async create(user: ISignInUserCreate): Promise<IUser> {
        return this.model.create({
            data: {
                username: user.username,
                email: user.email,
                password: user.password,
                salt: user.salt,
                role: user.role,
                account: {
                    create: {
                        currentBalance: user.currentBalance,
                        externalId: user.externalId,
                    },
                },
            },
        });
    }

    /**
     * Find one by selected fields
     * @override
     * @param fields
     * @param attributes
     * @returns {Promise<Partial<User>>}
     */
    async findOneByFields(
        fields: Partial<IUser>, attributes?: IUserAttributes,
    ): Promise<Partial<IUser>> {
        return prismaClient.user.findFirst({
            where: {
                ...fields,
            },
            select: {
                ...attributes,
                account: true,
            },
        });
    }
}

export default new UserRepository(
    prismaClient.user,
);
