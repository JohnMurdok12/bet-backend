import prismaClient from '../../prisma/client';
import AbstractRepository from '@repositories/abstract';
import IAccountOperation, { IAccountOperationAttributes } from '@interfaces/model/IAccountOperation';

class AccountOperationRepository extends AbstractRepository<IAccountOperation, IAccountOperationAttributes> {
}

export default new AccountOperationRepository(
    prismaClient.accountOperation,
);
