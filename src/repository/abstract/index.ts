import IPrismaFindManyFilters from '@interfaces/prisma/IPrismaFindManyFilters';

interface PrismaModel {
    findFirst: Function
    findMany: Function
    create: Function
    update: Function
}

class AbstractRepository<I extends object, IAttributes> {
    protected model: PrismaModel;


    constructor(model: PrismaModel) {
        this.model = model;
    }

    /**
     * Find one by selected fields
     * @param fields
     * @param attributes
     * @returns {Promise<Partial<User>>}
     */
    async findOneByFields(
        fields: Partial<I>, attributes?: IAttributes,
    ): Promise<Partial<I>> {
        return this.model.findFirst({
            where: {
                ...fields,
            },
            select: attributes,
        });
    }

    /**
     * Find entities by filters
     * @param filters
     * @returns {Promise<Partial<I>[]>}
     */
    findByFilters(filters: IPrismaFindManyFilters): Promise<Partial<I>[]> {
        return this.model.findMany(filters);
    }

    /**
     * Create action
     * @param data
     * @returns {Promise<I>}
     */
    async create(data: Partial<I>): Promise<I> {
        return this.model.create({ data });
    }

    /**
     * Update data by fields
     * @param data
     * @param fields
     * @returns {Promise<I>}
     */
    async updateByFields(data: Partial<I>, fields: Partial<I>): Promise<I> {
        return this.model.update({
            data,
            where: { ...fields },
        });
    }
}

export default AbstractRepository;
