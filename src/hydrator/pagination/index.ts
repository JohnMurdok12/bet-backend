import { IQueryPagination } from '@interfaces/request/query/IPagination';
import IPrismaPagination from '@interfaces/prisma/IPrismaPagination';

/**
 * Format IQueryPagination to IPrismaPagination
 */
export default (pagination: IQueryPagination): IPrismaPagination => {
    const request: IPrismaPagination = {
        take: pagination.limit,
        cursor: pagination.cursor,
        orderBy: pagination.orderBy,
    };

    if (request.cursor) {
        request.skip = 1;
    }

    return request;
};