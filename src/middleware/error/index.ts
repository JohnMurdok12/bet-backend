import httpStatus from 'http-status';
import { Response } from 'express';
import { IHttpError } from '../../error/http';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const errorMiddleware = async (err: IHttpError, req: null, res: Response, next: Function) => {
    const status = err.status || httpStatus.INTERNAL_SERVER_ERROR;
    const message = err.message;

    console.error(
        `[${status}] ${err.message} ${err.stack}`,
    );
    return res.status(status).send({ message, stack: err.stack });
};

export default errorMiddleware;
