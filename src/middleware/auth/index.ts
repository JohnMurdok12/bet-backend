import { Request, Response } from 'express';
import httpStatus from 'http-status';
import { decode as decodeToken } from '@utils/jwt';
import userService from '@services/user';
import IUser from '@interfaces/model/IUser';
import { RequestHeaders } from '@constants/header';
import HttpError from '../../error/http';

/**
 * Authentication middleware
 * @param req
 * @param res 
 * @param next 
 * @returns {Function}
 */
const authMiddleware = async (req: Request, res: Response, next: Function) => {
    const { [RequestHeaders.AUTHORIZATION]: bearer } = req.headers;
    const token = bearer?.replace('Bearer ', '');

    if (!token) {
        return next();
    }

    try {
        const { uuid, expiresAt } = decodeToken(token);
        const currentDateMs: number = new Date().getTime();
        const expireTime: number = new Date(expiresAt).getTime();

        if (currentDateMs > expireTime) {
            throw new HttpError(httpStatus.FORBIDDEN, 'Token has expired');
        }

        const user: Partial<IUser> = await userService.getByUuid(uuid);

        if (!user) {
            throw new HttpError(httpStatus.NOT_FOUND, 'User not found');
        }

        res.locals.user = user;
    } catch (e) {
        return next(e);
    }

    return next();
};

export default authMiddleware;
