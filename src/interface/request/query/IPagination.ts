export enum OrderBy {
    ASC = 'asc',
    DESC = 'desc',
}

export interface IQueryOrderBy {
    id?: OrderBy
}

export interface IQueryCursor {
    id: number
}

export interface IQueryPagination {
    limit: number
    cursor?: IQueryCursor
    orderBy?: IQueryOrderBy
}
