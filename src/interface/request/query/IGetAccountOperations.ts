import { AccountOperationType, AccountOperation } from '@prisma/client';
import { IQueryPagination } from './IPagination';

interface GetAccountOperationsFilters {
    operationType?: AccountOperationType
}

interface GetAccountOperationsInput {
    filters?: GetAccountOperationsFilters
    pagination?: IQueryPagination
}

interface GetAccountOperationsResponse {
    operations: Partial<AccountOperation>[]
    cursor: number
}

export { GetAccountOperationsResponse, GetAccountOperationsFilters };
export default GetAccountOperationsInput;
