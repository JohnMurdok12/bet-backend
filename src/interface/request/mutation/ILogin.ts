interface ILoginInput {
    username: string;
    password: string;
}

interface ILogin {
    token: string;
    expiresAt: Date;
}

export { ILogin };
export default ILoginInput;