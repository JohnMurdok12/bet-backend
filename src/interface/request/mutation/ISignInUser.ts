import { User, UserRole } from '@prisma/client';

interface ISignInUserInput {
    data: ISignInUserInputData
}

interface ISignInUserInputData extends Partial<User> {
    username: string;
    password: string;
    email: string;
    role?: UserRole;
    externalId?: string;
    currentBalance: number;
}

interface ISignInUserCreate extends ISignInUserInputData {
    role: UserRole;
    salt: string;
}

export { ISignInUserCreate, ISignInUserInputData };
export default ISignInUserInput;