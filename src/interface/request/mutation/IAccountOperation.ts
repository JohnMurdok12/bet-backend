import { AccountOperation, AccountOperationType } from '@prisma/client';

interface IAccountOperationCreateInput {
    data: IAccountOperationCreateInputData
}

interface IAccountOperationCreateInputData extends Partial<AccountOperation> {
    operationType: AccountOperationType;
    amount: number;
}


export { IAccountOperationCreateInputData };
export default IAccountOperationCreateInput;