import { AccountOperation, AccountOperationType } from '@prisma/client';
import IPrismaFindManyFilters, { IPrismaWhere } from '@interfaces/prisma/IPrismaFindManyFilters';

interface IAccountOperationsWhere extends IPrismaWhere {
    accountId: number | {
        in: number[]
    }
    operationType?: {
        in: AccountOperationType[]
    }
}

interface IAccountOperationsFilters extends IPrismaFindManyFilters {
    where: IAccountOperationsWhere
}

interface IAccountOperationAttributes {
    id?: boolean;
    accountId?: boolean
    amount?: boolean
    operationType?: boolean
    createdAt?: boolean
}

export { IAccountOperationAttributes, IAccountOperationsFilters };
export default AccountOperation;