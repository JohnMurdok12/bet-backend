import { User, Account } from '@prisma/client';

interface IUser extends User {
    account: Account
}

interface IUserAttributes {
    id?: boolean;
    uuid?: boolean
    salt?: boolean
    role?: boolean
    email?: boolean
    username?: boolean
    password?: boolean
    createdAt?: boolean
    updatedAt?: boolean
}

export { IUserAttributes };
export default IUser;