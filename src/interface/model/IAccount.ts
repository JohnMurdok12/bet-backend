import { Account } from '@prisma/client';

interface IAccountAttributes {
    id?: boolean;
    userId?: boolean
    currentBalance?: boolean
    createdAt?: boolean
    updatedAt?: boolean
    externalId?: boolean
}

export { IAccountAttributes };
export default Account;