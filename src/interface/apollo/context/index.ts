import IUser from '@interfaces/model/IUser';

export default interface IApolloContext {
    user: Partial<IUser>
}
