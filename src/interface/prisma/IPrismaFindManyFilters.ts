import IPrismaPagination from './IPrismaPagination';

export interface IPrismaWhere {
    id?: number
}

export interface IPrismaSelect {
    id?: boolean
}

export default interface IPrismaFindManyFilters extends IPrismaPagination {
    where?: IPrismaWhere
    select?: IPrismaSelect
}