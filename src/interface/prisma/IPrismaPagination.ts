import { IQueryCursor, IQueryOrderBy } from '@interfaces/request/query/IPagination';

export default interface IPrismaPagination {
    take?: number
    cursor?: IQueryCursor
    skip?: number
    orderBy?: IQueryOrderBy
}