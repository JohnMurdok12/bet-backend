import * as crypto from 'crypto';

const {
    PASSWORD_LENGTH,
    SALT_LENGTH,
    PASSWORD_ITERATION,
} = process.env;

const DIGEST = 'sha256';
const STRING_ENCODING = 'hex';

export interface IPassword {
    password: string;
    salt: string;
}

/**
 * Has password function
 * @param {Strinng} password
 * @returns {IPassword}
 */
export const hashPassword = (password: string): IPassword => {
    const salt = crypto.randomBytes(Number(SALT_LENGTH)).toString(STRING_ENCODING);
    const hashedPassword = crypto.pbkdf2Sync(
        password,
        salt,
        Number(PASSWORD_ITERATION),
        Number(PASSWORD_LENGTH),
        DIGEST,
    ).toString(STRING_ENCODING);
        
    return { password: hashedPassword, salt };
};

export const checkPassword = (password: string, storedPassword: string, salt: string): Boolean => {
    const hashedPassword = crypto.pbkdf2Sync(
        password,
        salt,
        Number(PASSWORD_ITERATION),
        Number(PASSWORD_LENGTH),
        DIGEST,
    ).toString(STRING_ENCODING);

    return hashedPassword === storedPassword;
};
