import jwt from 'jsonwebtoken';
import { ILogin } from '@interfaces/request/mutation/ILogin';
const DIGEST = 'HS256';

export interface IDecodedToken {
    uuid: string;
    expiresAt: string;
}

export const encode = (uuid: string): ILogin => {
    const expiresAt = new Date();
    expiresAt.setDate(expiresAt.getDate() + 1);

    const token = jwt.sign(
        {
            uuid,
            expiresAt: expiresAt.toISOString(),
        },
        process.env.JWT_SECRET,
        {
            algorithm: DIGEST,
        },
    );

    return { token, expiresAt };
};

export const decode = (token: string): IDecodedToken => jwt.verify(token, process.env.JWT_SECRET) as IDecodedToken;
