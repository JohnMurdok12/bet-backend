import express from 'express';
import apolloServer from './apollo';
import authMiddleware from './middleware/auth';
import errorMiddleware from './middleware/error';

const app = express();

/**
 * Main entry point
 */
const main = async (): Promise<void> => {
    try {
        await apolloServer.start();
        app.use(authMiddleware);
        app.use('/graphql', apolloServer.getMiddleware({ path: '/' }));
        app.use(errorMiddleware);

        app.listen({
            port: process.env.SERVER_PORT,
        }, () => {
            console.log(`Server ready on ${process.env.SERVER_PORT}`);
        });
    } catch (e) {
        console.error(e);
        process.exit(1);
    }
};

export default main();
