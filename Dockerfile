FROM node:16.6.2

WORKDIR /var/www
ADD './' ./

RUN npm install -g pm2
RUN npm install --loglevel warn
RUN npm run build
RUN rm -rf src

EXPOSE 3000
CMD [ "npx", "pm2-runtime", "ecosystem.config.prod.js" ]
