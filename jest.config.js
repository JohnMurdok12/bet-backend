const { pathsToModuleNameMapper } = require('ts-jest/utils');
const { compilerOptions } = require('./tsconfig');

module.exports = {
    preset: 'ts-jest',
    clearMocks: true,
    verbose: true,
    cacheDirectory: './node_modules/.cache/jest',
    testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/dist/'],
    setupFiles: ['<rootDir>/src/test/jest.setup.ts'],
    moduleNameMapper: {
        "@utils/(.*)": "<rootDir>/src/util/$1",
        "@resolvers/(.*)": "<rootDir>/src/apollo/resolver/$1",
        "@repositories/(.*)": "<rootDir>/src/repository/$1",
        "@services/(.*)": "<rootDir>/src/service/$1",
        "@interfaces/(.*)": "<rootDir>/src/interface/$1",
        "@middlewares/(.*)": "<rootDir>/src/middleware/$1",
        "@constants/(.*)": "<rootDir>/src/constant/$1",
    },
};
